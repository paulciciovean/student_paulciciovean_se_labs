import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class AsistentEmotional  implements ActionListener {

	private JButton happy,sad,fear,disgust,anger,surprise;
	private JButton intensity1,intensity2,intensity3,intensity4,intensity5;
	private JTextArea fisier;
	
	AsistentEmotional()
	{
		JFrame frame=new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		frame.setSize(700,700);
		frame.setTitle("Asistent Emotional Personal");
		frame.setResizable(false);
		frame.setVisible(true);
		
		JPanel panel=new JPanel();
		panel.setLayout(null);
		frame.add(panel);
		
		happy=new JButton("happiness");
		happy.setBounds(25,25,100,35);
		panel.add(happy);
		
		sad=new JButton("sadness");
		sad.setBounds(130,25,100,35);
		panel.add(sad);
		
		fear=new JButton("fear");
		fear.setBounds(235,25,100,35);
		panel.add(fear);
		
		disgust=new JButton("disgust");
		disgust.setBounds(340,25,100,35);
		panel.add(disgust);
		
		anger=new JButton("anger");
		anger.setBounds(445,25,100,35);
		panel.add(anger);
		
		surprise=new JButton("surprise");
		surprise.setBounds(550,25,100,35);
		panel.add(surprise);
		
		intensity1=new JButton("1");
		intensity1.setBounds(25, 150, 70, 30);
		panel.add(intensity1);
		
		intensity2=new JButton("2");
		intensity2.setBounds(25,200,70,30);
		panel.add(intensity2);
		
		intensity3=new JButton("3");
		intensity3.setBounds(25,250,70,30);
		panel.add(intensity3);
		
		intensity4=new JButton("4");
		intensity4.setBounds(25,300,70,30);
		panel.add(intensity4);
		
		intensity5=new JButton("5");
		intensity5.setBounds(25, 350, 70, 30);
		panel.add(intensity5);
		
		fisier=new JTextArea();
		fisier.setBounds(450,450,100,150);
		panel.add(fisier);
		
		happy.addActionListener(this);
		sad.addActionListener(this);
		fear.addActionListener(this);
		disgust.addActionListener(this);
		anger.addActionListener(this);
		surprise.addActionListener(this);
		intensity1.addActionListener(this);
		intensity2.addActionListener(this);
		intensity3.addActionListener(this);
		intensity4.addActionListener(this);
		intensity5.addActionListener(this);
		
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==happy)
		{
			this.fisier.append("hapiness ");
		}
		if(e.getSource()==sad)
		{
			this.fisier.append("sadness ");
		}
		if(e.getSource()==fear)
		{
			this.fisier.append("fear ");
		}
		if(e.getSource()==disgust)
		{
			this.fisier.append("disgust ");
		}
		if(e.getSource()==anger)
		{
			this.fisier.append("anger ");
		}
		if(e.getSource()==surprise)
		{
			this.fisier.append("surprise ");
		}
		if(e.getSource()==intensity1)
		{
			this.fisier.append("1\n");
		}
		if(e.getSource()==intensity2)
		{
			this.fisier.append("2\n");
		}
		if(e.getSource()==intensity3)
		{
			this.fisier.append("3\n");
			
		}
		if(e.getSource()==intensity4)
		{
			this.fisier.append("4\n");
		}
		if(e.getSource()==intensity5)
		{
			this.fisier.append("5\n");
		}
				
			
			
			
	}
	
	public static void main(String[] args) {
		AsistentEmotional a1=new AsistentEmotional();

	}

}
