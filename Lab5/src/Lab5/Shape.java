package Lab5;

abstract public class Shape {
	protected String color;
	protected boolean filled;
	Shape()
	{
		
	}
	Shape(String c,boolean f)
	{
		this.color=c;
		this.filled=f;
	}
	public String getColor()
	{
		return color;
	}
	public void setColor(String color)
	{
		this.color=color;
		
	}
	public boolean isfilled()
	{
		return filled;
	}
	public void setFilled()
	{
		this.filled=filled;
	}
	abstract double getArea();
	abstract double getPerimeter();
	abstract String toString();
	
}
