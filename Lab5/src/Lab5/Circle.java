package Lab5;

 public class Circle extends Shape {
	double radius;
	Circle()
	{
		
	}
	Circle(double radius)
	{
		this.radius=radius;
	}
	Circle(double r , String c , boolean f )
	{
		this.radius=r;
		this.color=c;
		this.filled=f;
	}
	public double getRadius()
	{
		return radius;
	}
	public void SetRadius(double r)
	{
		this.radius=r;
	}
	public double getArea()
	{
		return 3.14*(radius*radius);
	}
	public double getPerimeter()
	{
		return 2*3.14*radius;
	}
	public String toString()
	{
		return "Cerc cu raza=" + this.radius + " si culoare= " + this.getColor() + " si " +this.filled;
	}
}
