package Lab5;

public class ex1test {

	public static void main(String[] args) {
		Shape s;
		s=new Circle(3,"Albastru",true);
		System.out.println("Aria cercului este: "+s.getArea());
		System.out.println("Perimetrul cercului este: "+s.getPerimeter());
		System.out.println(s.toString());
		System.out.println();
		s=new Rectangle(5,7,"Mov",false);
		System.out.println("Aria dreptunghiului este: "+s.getArea());
		System.out.println("Perimetrul dreptunghiului este: "+s.getPerimeter());
		System.out.println(s.toString());
		System.out.println();
		s=new Square(12,"Galben",true);
		System.out.println("Aria patratului este: "+s.getArea());
		System.out.println("Perimetrul patratului este: "+s.getPerimeter());
		System.out.println(s.toString());
		System.out.println();
	}

}
