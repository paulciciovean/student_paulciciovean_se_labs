package Lab5;

public class Rectangle extends Shape {
	double width;
	double length;
	Rectangle()
	{
		
	}
	Rectangle(double w, double l)
	{
		this.width=w;
		this.length=l;
	}
	Rectangle(double w,double l, String c, boolean f)
	{
		this.width=w;
		this.length=l;
		this.color=c;
		this.filled=f;
	}
	public double getWidth()
	{
		return width;
	}
	public void SetWidth(double w)
	{
		this.width=w;
	}
	public double getLength()
	{
		return length;
	}
	public void SetLength(double l)
	{
		this.length=l;
	}
	double getArea()
	{
		return width*length;
	}
	double getPerimeter() {
		return 2*width+2*length;
	}
	String toString()
	{
		return "Rectangle with length=" + this.length + " and width= " + this.width + " color=" + this.getColor() + " and " + filled;
	}
	
}
