package ex3;

import java.util.Random;

public class LightSensor extends Sensor {
	public int readValue()
	{
		Random r = new Random();
		int low = 0;
		int high = 100;
		int result = r.nextInt(high-low) + low;
		return result;
	}

}
