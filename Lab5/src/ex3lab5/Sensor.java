package ex3;

public abstract class Sensor {
	String location;
	abstract int readValue();
	String getLocation()
	{
		return this.location;
	}

}
