public class Counters2 extends Thread
{
	Counters2(String name)
	{
		super(name);
	}
	
	public void run()
	{
		for(int i=0;i<100;++i)
		{
            System.out.println(getName()+"i="+i);
            try 
            {
            	Thread.sleep((int)(Math.random()*200));
            } 
            catch(InterruptedException e) 
            {
            	e.printStackTrace();
            }
            
		}
		System.out.println("first counter done");
	}
	
	public void run2()
	{
		for(int i=100;i<=200;++i)
		{
            System.out.println(getName()+"i="+i);
            try 
            {
            	Thread.sleep((int)(Math.random()*200));
            } 
            catch(InterruptedException e) 
            {
            	e.printStackTrace();
            }
		}
		System.out.println("second counter done");
		
	}
	
	public static void main(String[] args) 
	{
		Counters2 n1=new Counters2("Counter 1: ");
        Counters2 n2=new Counters2("Coutner2 2: ");
        n1.run();
        n2.run2();
	}
}
