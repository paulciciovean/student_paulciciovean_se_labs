package ex2;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Counter implements ActionListener{
	int cont=0;
	JLabel label;
	JButton button;
	JFrame frame;
	Counter(){
	    frame = new JFrame();
		button=new JButton();
		label=new JLabel();
		frame.setTitle("Counter");
		frame.setVisible(true);
		frame.setSize(500, 300);
		button.setText("Click here!");
		button.setVisible(true);
		button.setSize(150, 50);
		button.addActionListener(this);
		label.setSize(150, 50);
		label.setVisible(true);
		label.setText("Counting..."  );
		frame.add(button);
		frame.add(label);
	}
	public void actionPerformed(ActionEvent e)
	{
		cont++;
		label.setText("Counting..." + this.cont );
	}
	public static void main(String[] args) {
		
		Counter c= new Counter();

	}

}
