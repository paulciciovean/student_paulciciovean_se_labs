package AllExercises;

public class ex6l2 {

	static int factorial_nr(int n)
	{
		int rez;
		rez=1;
		for(int i=1;i<=n;i++)
		{
			rez=rez*i;
		}
		return rez;
	}
	static int factorial_r(int n)
	{
		if (n == 0)    
		    return 1;    
		  else    
		    return(n * factorial_r(n-1));    
	}
	public static void main(String[] args) {
		int N;
		N=5;
		System.out.println(factorial_nr(N));
		System.out.println(factorial_r(N));
	}

}
