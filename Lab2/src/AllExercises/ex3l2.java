package AllExercises;

import java.util.Scanner;
public class ex3l2 {

	static boolean prim(int a)
	{
		for(int i=2;i<=a/2;i++)
		{
			if(a%i==0)
				return false;
		}
		return true;
	}
	public static void main(String[] args) {
		Scanner cin=new Scanner(System.in);
		int A=cin.nextInt();
		int B=cin.nextInt();
		for(int i=A;i<=B;i++)
		{
			if(prim(i)==true)
				System.out.println(" " + i);
		}

	}

}
