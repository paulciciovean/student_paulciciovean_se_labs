
public class Author {
	private String name,email;
	private char gender;
	public Author(String n,String e,char g)
	{
		this.name=n;
		this.email=e;
		this.gender=g;
	}
	public String getName()
	{
		return this.name;
	}
	public String getEmail()
	{
		return this.email;
	}
	public String setEmail(String newemail)
	{
		this.email=newemail;
		return this.email;
	}
	public char getGender()
	{
		return this.gender;
	}
	public String toString()
	{
		return (this.name + "(" + this.gender + ")" + " at " + this.email);
	}

}
