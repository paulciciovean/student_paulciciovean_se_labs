
public class TestsClases {

	public static void main(String[] args) {
		///Test for circle :
		Circle c1=new Circle(13);
		System.out.println("Cercul c1 are raza " + c1.radius + ", culoarea " + c1.color + " si aria " + c1.getArea()  );
		
		///Test for Author :
		Author a1=new Author("Ciciovean Paul", "ciciovean_paul@yahoo.com" ,'m');
		System.out.println(a1.toString());
		
		///Test for Book
		Book b1=new Book("Baltagul+",a1,50.99,5);
		System.out.println("Cartea b1 are urm informatii : " + b1.toString());
		System.out.println("Cartea costa " + b1.price + " si sunt disponibile " + b1.InStock + " in stoc");
		
		///Test for Cylinder
		
		Cylinder c2=new Cylinder(5,8);
		System.out.println("Aria cilindrului este : " + c2.getArea2() );
	}

}
