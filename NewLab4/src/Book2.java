
public class Book2 {
	

	String name;
	Author[] authors;
	double price;
	int InStock;
	public Book2(String n , Author[] a , double p)
	{
		this.name=n;
		this.authors=a;
		this.price=p;
	}
	public Book2(String n,Author[] a,double p,int is)
	{
		this.name=n;
		this.authors=a;
		this.price=p;
		this.InStock=is;
	}
	public String getName()
	{
		return this.name;
	}
	public Author[] getAuthor()
	{
		return this.authors;
	}
	public double getPrice()
	{
		return this.price;
		
	}
	public void setPrice(double newprice)
	{
		this.price=newprice;

	}
	public int getInStock()
	{
		return this.InStock;
	}
	public void setInStock(int newInStock)
	{
		this.InStock=newInStock;
	}
	public String toString()
	{
		return ( this.name + "by" + this.authors.toString() );
	}

}
