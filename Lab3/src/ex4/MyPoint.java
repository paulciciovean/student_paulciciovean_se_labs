package ex4;

import java.lang.Math; 

public class MyPoint {
	int x,y;
	MyPoint()
	{
		this.x=0;
		this.y=0;
	}
	MyPoint(int x, int y)
	{
		this.x=x;
		this.y=y;
	}
	public int getX()
	{
		return this.x;
	}
	public int getY()
	{
		return this.y;
	}
	public int setX(int x)
	{
		this.x=x;
		return x;
	}
	public int setY(int y)
	{
		this.y=y;
		return y;
	}
	public double distance(int x,int y)
	{
	
		return ( Math.sqrt((x-this.x)*(x-this.x)+(y-this.y)*(y-this.y)) );
	}

}
