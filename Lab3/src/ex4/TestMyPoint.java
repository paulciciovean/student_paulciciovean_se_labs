package ex4;

public class TestMyPoint {

	public static void main(String[] args) {
		MyPoint p1=new MyPoint(3,7);
		MyPoint p2=new MyPoint(4,2);
		System.out.println("Punctul p1 are coordonatele : " + p1.x + " , " + p1.y);
		System.out.println("Punctul p2 are coordonatele : " + p2.x + " , "+ p2.y);
		double dist;
		dist=p1.distance(p2.x , p2.y);
		System.out.println("Distanta dintre p1 si p2 este : " + dist);
	}

}
