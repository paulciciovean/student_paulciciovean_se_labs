package ex2;

public class Circle {
	double radius;
	String color;
	Circle()
	{
		this.radius=1.0;
		this.color="red";
	}
	Circle(double r,String c)
	{
		this.radius=r;
		this.color=c;
	}
	public double getRadius() 
	{
		return this.radius;
	}
	public double getArea()
	{
		return 3.14*this.radius*this.radius;
	}

}
