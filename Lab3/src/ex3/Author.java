package ex3;

public class Author {
	String name,email;
	char gender;
	public Author(String n,String e,char g)
	{
		this.name=n;
		this.email=e;
		this.gender=g;
	}
	public String getName()
	{
		return this.name;
	}
	public String getEmail()
	{
		return this.email;
	}
	public String setEmail(String new_email)
	{
		this.email=new_email;
		return this.email;
	}
	public char getGender()
	{
		return this.gender;
	}
	public String toString()
	{
		 return (this.name + "("+this.gender + ")" + " at " + this.email);
	}

}
