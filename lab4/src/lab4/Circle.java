package lab4;

public class Circle {

	double radius=1.0;
	String color="red";
	Circle(double r)
	{
		radius=r;
	}
	public double getRadius()
	{
		return radius;
	}
	public double getArea(double radius)
	{
		return (3.14*radius*radius);
	}
	public static void main(String[] args) 
	{
		Circle c1=new Circle(3);
		Circle c2=new Circle(2.5);
		System.out.println("Aria cercului c1 este " + c1.getArea(c1.radius));
		System.out.println("Aria cercului c2 este " + c2.getArea(c2.radius));
		
	}
}
