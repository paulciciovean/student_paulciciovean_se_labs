package lab4;

public class Author {
	
    String Name,email;
    char gender;
    Author(String n , String e , char g)
    {
    	Name=n;
    	email=e;
    	gender=g;
    }
    public String getName()
    {
    	return Name;
    }
    public String getEmail()
    {
    	return email;
    }
    public char getGender() {
    	return gender;
    }
    public String toString()
    {
    	return (this.Name +  this.gender + this.email);
    }
    public static void main(String[] args) 
	{
		Author a1=new Author("Popescu","popescu@yahoo.com",'m');
		System.out.println("Informatiile despre autorul a1 sunt : " + a1.toString());
		
	}
}
