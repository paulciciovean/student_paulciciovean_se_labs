package prb1;

import javax.swing.*;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;


public class GuessNumber implements ActionListener{
	
	private int storenumber;
	private JTextField text;
	private JTextArea area;
	private JButton button1,button2;
	public static int getRandom()
	{
		int random;
		Random generator = new Random();
        random = generator.nextInt(100) + 1;
        return random;
	}
	GuessNumber()
	{
		JFrame frame= new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		frame.setSize(500,500);
		frame.setTitle("Guess the number");
		frame.setResizable(false);
		frame.setVisible(true);
		
		JPanel panel=new JPanel();
		panel.setLayout(null);
		frame.add(panel);
		
		
		button1=new JButton("OK");
		button1.setBounds(200,100,80,35);
		panel.add(button1);
		
		button2=new JButton();
		button2.setBounds(300,350,150,35);
		button2.setText("New Game");
		panel.add(button2);
		
		text= new JTextField();
		text.setBounds(70,100,80, 35);
		panel.add(text);
		
        area=new JTextArea();
		area.setBounds(70, 150, 80, 160);
		panel.add(area);
		
		button1.addActionListener(this);
		
		
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==button1)
		{
		storenumber = Integer.parseInt(text.getText());
		if(storenumber>getRandom())
		{
			this.area.append("Greater\n");
		}
		else if(storenumber<getRandom())
		{
			this.area.append("Smaller\n");
		}
		else this.area.append("Bravo\n");
		}
		if(e.getSource()==button2)
		{
			this.area.append("Restart\n");
		}
			
	}
	public static void main(String[] args) {
		GuessNumber g1=new GuessNumber();
	}
	

}
